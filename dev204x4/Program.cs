﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x4
{
    class Program
    {
        static void Main(string[] args)
        {
            object[] structs = new object[5];
            structs[0] = new student { firstname = "John", lastname = "Smith", birthday = "1991-01-01"};
            structs[1] = new teacher { firstname = "Jane", lastname = "Doe", birthday = "1970-01-01" };
            structs[2] = new program { name = "BSc in Biology" };
            structs[3] = new course { code = "BIO101", name = "Introductory Biology" };
            structs[4] = new course { code = "BIO102", name = "Introductory Biology 2" };
            Console.WriteLine("Student name is: {0} {1}. His/her birthday is: {2}", (((student)structs[0]).firstname), (((student)structs[0]).lastname), (((student)structs[0]).birthday));
            Console.WriteLine("Teacher name is: {0} {1}", (((teacher)structs[1]).firstname), (((teacher)structs[1]).lastname), (((teacher)structs[1]).birthday));
            Console.WriteLine("Program name is: {0}.", (((program)structs[2]).name));
            Console.WriteLine("Course 1 is: {0} - {1}.", (((course)structs[3]).code), (((course)structs[3]).name));
            Console.WriteLine("Course 2 is: {0} - {1}.", (((course)structs[4]).code), (((course)structs[4]).name));
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
    }

    public struct student
    {
        public string firstname;
        public string lastname;
        public string birthday;        
    };

    public struct teacher
    {
        public string firstname;
        public string lastname;
        public string birthday;
        public string faculty;
    };

    public struct program
    {
        public string name;
    };

    public struct course
    {
        public string code;
        public string name;
    };
}
